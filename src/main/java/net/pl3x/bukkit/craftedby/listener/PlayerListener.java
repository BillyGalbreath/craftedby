package net.pl3x.bukkit.craftedby.listener;

import net.pl3x.bukkit.craftedby.Logger;
import net.pl3x.bukkit.craftedby.configuration.Config;
import net.pl3x.bukkit.craftedby.configuration.Lang;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PlayerListener implements Listener {
    private static final Set<Material> allowedItems = new HashSet<>();

    public PlayerListener() {
        for (String name : Config.CRAFTED_ITEMS.getStringList()) {
            Material material = Material.getMaterial(name);
            if (material == null) {
                continue;
            }
            allowedItems.add(material);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerCraft(CraftItemEvent event) {
        ItemStack result = event.getCurrentItem();

        if (!allowedItems.contains(result.getType())) {
            return; // not allowed item
        }

        String playerName = event.getWhoClicked().getName();

        ItemMeta meta = result.getItemMeta();
        List<String> lore = meta.getLore();
        if (lore == null) {
            lore = new ArrayList<>();
        }
        lore.add("");
        Collections.addAll(lore, Lang.LORE.replace("{player}", playerName).split("\n"));
        meta.setLore(lore);
        result.setItemMeta(meta);

        Logger.debug("Lore was crafted on " + result.getType().name() + " by " + playerName);
    }
}
