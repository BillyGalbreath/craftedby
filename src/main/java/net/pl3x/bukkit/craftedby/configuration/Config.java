package net.pl3x.bukkit.craftedby.configuration;

import net.pl3x.bukkit.craftedby.CraftedBy;

import java.util.List;

public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml"),
    CRAFTED_ITEMS(null);

    private final CraftedBy plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = CraftedBy.getPlugin();
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    public List<String> getStringList() {
        return plugin.getConfig().getStringList(getKey());
    }
}
