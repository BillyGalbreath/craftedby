package net.pl3x.bukkit.craftedby;

import net.pl3x.bukkit.craftedby.configuration.Lang;
import net.pl3x.bukkit.craftedby.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class CraftedBy extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static CraftedBy getPlugin() {
        return CraftedBy.getPlugin(CraftedBy.class);
    }
}
